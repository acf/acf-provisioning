<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if viewlibrary.check_permission("detaildevices") then %>
<script type="text/javascript">
	window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/detaildevices");
</script>
<% end %>

<%
local func = haserl.loadfile(page_info.viewfile:gsub("listdevices", "detaildevices"))
func(view, viewlibrary, page_info, session)
%>
