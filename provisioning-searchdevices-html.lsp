<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if viewlibrary.check_permission("searchdevicedetails") then %>
<script type="text/javascript">
        window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/searchdevicedetails");
</script>
<% end %>

<%
local func = haserl.loadfile(page_info.viewfile:gsub("searchdevices", "searchdevicedetails"))
func(view, viewlibrary, page_info, session)
%>
