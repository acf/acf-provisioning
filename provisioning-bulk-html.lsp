<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	if viewlibrary.check_permission("bulkcreatedevices") then
		viewlibrary.dispatch_component("bulkcreatedevices")
	end
end %>

<%
	local viewtype = cfe({type="hidden", value="stream"})
	if viewlibrary.check_permission("bulkdumprawdevices") then
		htmlviewfunctions.displayitem(cfe({type="form", value={viewtype=viewtype}, label="Bulk Dump Devices", option="Dump", action="bulkdumprawdevices"}), page_info)
	elseif viewlibrary.check_permission("bulkdumpdevices") then
		htmlviewfunctions.displayitem(cfe({type="form", value={viewtype=viewtype}, label="Bulk Dump Devices", option="Dump", action="bulkdumpdevices"}), page_info)
	end
%>
