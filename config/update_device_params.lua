-- This is the script run after editing device params
local functions, params, oldparams = ...

posix = require("posix")

local root = "/var/www/provisioning/htdocs/"

--functions.logevent("got to update_device_params script")

local function findip(mac)
	if not mac or mac == "" then
		return nil
	end
	local ipaddr = functions.getselectresponse("SELECT ip FROM provisioning_requests WHERE mac~*'"..functions.escape(mac).."'")
	if ipaddr and ipaddr[1] then
		return ipaddr[1].ip
	end
end

local notify_device = function(mac, extension)
	local ipaddr = findip(mac)
	if ipaddr then
		--functions.logevent("Notifying "..ipaddr.." to update for "..(mac or ""))
		os.execute("/etc/provisioning/notify_device "..ipaddr.." "..extension)
	else
		--functions.logevent("Warning - could not find IP address for "..(mac or ""))
	end
end

oldparams = oldparams or {value={}}

-- If the mac address changed for Polycom with valid MAC (not blank or all 0's), we need to move the associated config files
if oldparams.value.device and oldparams.value.device.value.mac and oldparams.value.device.value.mac.value ~= "" and params.value.device and params.value.device.value.mac and params.value.device.value.mac.value ~= oldparams.value.device.value.mac.value then
	if string.match(oldparams.value.device.label, "Polycom") and string.match(oldparams.value.device.value.mac.value, "[1-9A-F]") then
		local deletefiles = true
		if string.match(params.value.device.value.mac.value, "[1-9A-F]") then
			--functions.logevent("Moving files for "..oldparams.value.device.value.mac.value)
			deletefiles = false
		else
			--functions.logevent("Deleting files for "..oldparams.value.device.value.mac.value)
		end
		local path = root.."Polycom/"
		if posix.stat(path, "type") == "directory" then
	                for d in posix.files(path) do
        	                if string.match(d, string.lower(oldparams.value.device.value.mac.value)) and posix.stat(path..d, "type") == "regular" then
                	                local newfile = string.gsub(d, string.lower(oldparams.value.device.value.mac.value), string.lower(params.value.device.value.mac.value))
					if deletefiles then
	                        	        --functions.logevent("deleting "..path..d)
        	                        	os.remove(path..d)
					else
	                        	        --functions.logevent("moving "..path..d.." to "..path..newfile)
        	                        	os.rename(path..d, path..newfile)
					end
				end
                        end
                end
        end
end

-- Notify the phone to pull it's config
-- Try to get a valid extension currently on the device
local oldexten = ""
for name,val in pairs(oldparams.value) do
	if string.match(name, "^reg") and val.value.extension and val.value.extension.value ~= "" then
		oldexten = val.value.extension.value
		break
	end
end
if params.value.device and params.value.device.value.mac and params.value.device.value.mac.value ~= "" then
	notify_device(params.value.device.value.mac.value, oldexten)
end
if oldparams.value.device and oldparams.value.device.value.mac and oldparams.value.device.value.mac.value ~= "" then
	notify_device(oldparams.value.device.value.mac.value, oldexten)
end
