-- This is the script run to determine the device class from the HTTP user agent
local functions, classes, agent, path = ...

--functions.logevent("got to determine_class script")

local manufacture, model

if string.match(agent, "Polycom") then
	manufacture = "Polycom"
	model = string.match(agent, "Polycom[^%s%d]+(%d+)")
elseif string.match(agent, "Linksys") then
	manufacture = "Linksys"
	model = string.match(agent, "Linksys/(%P+)")
elseif string.match(agent, "Cisco") then
	manufacture = "Cisco"
	model = string.match(agent, "Cisco/(%P+)")
elseif string.match(agent, "snom") then
	manufacture = "Snom"
	model = string.match(agent, "snom(%d+)")
elseif string.match(agent, "Grandstream") then
	manufacture = "Grandstream"
	model = string.match(agent, "Grandstream[^%d]+(%d+)")
elseif string.match(agent, "Algo") then
	manufacture = "Algo"
	model = string.match(agent, "Algo[^%d]+(%d+)")
elseif string.match(agent, "^Wget$") then
	manufacture = "CyberData"
	model = "Intercom"
end

--functions.logevent("Found "..(manufacture or "").." model "..(model or ""))

if manufacture and model then
	for i,c in ipairs(classes.value) do
		if string.match(c.label, manufacture) and string.match(c.label, model) then
			return c.class_id, c.name
		end
	end
end
