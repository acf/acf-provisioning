-- This is the script run to process uploaded config files
local functions, mac, data, device_id = ...

--functions.logevent("got to process_put script")

local function findip(mac)
	if not mac or mac == "" then
		return nil
	end
	local ipaddr = functions.getselectresponse("SELECT ip FROM provisioning_requests WHERE mac~*'"..functions.escape(mac).."'")
	if ipaddr and ipaddr[1] then
		return ipaddr[1].ip
	end
end

local notify_device = function(mac, extension)
	local ipaddr = findip(mac)
	if ipaddr then
		--functions.logevent("Notifying "..ipaddr.." to update for "..(mac or ""))
		os.execute("/etc/provisioning/notify_device "..ipaddr.." "..extension)
	else
		--functions.logevent("Warning - could not find IP address for "..(mac or ""))
	end
end

-- Get the params
local params = functions.get_device_params(device_id)

function process_polycom()
	local before, xml, after = string.match(data, "(.*<OVERRIDES)(.*)(/>.*)")
	if not xml then return end
	local attrs = {}
	for str in string.gmatch(xml, "%S+=\"[^\"]*\"") do
		local n,v = string.match(str, "([^=]+)=\"(.*)\"")
		if not attrs[n] then
			attrs[n] = v
		end
	end

--	for n,v in pairs(attrs) do
--		functions.logevent("name "..n.." val "..(v or ""))
--	end

	-- Read attributes from attrs and generate paramaters for ACF
	local xmlout = {}
	for n,v in pairs(attrs) do

		-- this attribute controls the default ringtone, when the phone have only one reg
		if n == "np.normal.ringing.calls.tonePattern" then
			params.value.reg1.value.polycomringtone.value = string.match(v, "%d+")

		-- this attribute controls the display language
		elseif n == "lcl.ml.lang" then
			params.value.device.value.displaylang.value = v

		-- found any attribute related with 24HourClock
		elseif string.find(n, "24HourClock") then
			params.value.device.value.militarytimeenable.value = (v == "1")

		-- this attribute enables local clock
		elseif string.find(n, "localClockEnabled") then
			params.value.device.value.clockenable.value = (v == "1")

		-- this attribute enables screen saver
		elseif n == "up.screenSaver.enabled" then
			params.value.device.value.screensaverenable.value = (v == "1")

		-- this attribute enables call waiting (for the entire device, not per line)
		elseif n == "call.callWaiting.enable" then
			params.value.services.value.callwaitingenable.value = (v == "1")

		else
			-- search attribute name for reg_name like "reg.1."
			--  and for rest like "fwdStatus"
			local num, rest = string.match(n, "reg%.(%d+)%.([%.%a]*)$")

			-- if rest is defined, then num is defined also
			if rest == "fwd.busy.contact" then
				params.value["reg"..num].value.forwardbusy.value = v
			elseif rest == "fwd.noanswer.contact" then
				params.value["reg"..num].value.forwardnoanswer.value = v
			elseif rest == "fwdContact" then
				params.value["reg"..num].value.forwardall.value = v
			elseif rest == "fwd.busy.status" then
				params.value["reg"..num].value.forwardbusyenable.value = (v == "1")
			elseif rest == "fwd.noanswer.status" then
				params.value["reg"..num].value.forwardnoanswerenable.value = (v == "1")
			elseif rest == "fwdStatus" then
				params.value["reg"..num].value.forwardallenable.value = (v == "1")
			elseif rest == "ringType" then
				params.value["reg"..num].value.polycomringtone.value = string.match(v, "%d+")
			else
				xmlout[#xmlout+1] = n.."=\""..v.."\""
			end
		end
	end

	data = before.." "..table.concat(xmlout, " ").." "..after

end

-- Determine the template
if string.match(params.value.device.label, "Polycom") then
	process_polycom()
end

-- Update the params
local params = functions.set_device_params(params)

-- If the update fails, notify the phone to revert the config change
if params.errtxt and params.value.device and params.value.device.value.mac then
	exten = ""
	if params.value.reg1 and params.value.reg1.value.extension then
		exten = params.value.reg1.value.extension.value
	end
	notify_device(params.value.device.value.mac.value, exten)
end

-- Return the updated data
return data
