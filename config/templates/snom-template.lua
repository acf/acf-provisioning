<%
local values = ...

--[[ -- Parameters
values["device"]["adminpassword"] = ""
-- Unimplemented -- values["device"]["digitmap"] = ""
-- Unimplemented -- values["device"]["digitmaptimeout"] = ""
values["device"]["forwardall"] = ""
values["device"]["forwardallenable"] = false
values["device"]["forwardbusy"] = ""
values["device"]["forwardbusyenable"] = false
values["device"]["forwardnoanswer"] = ""
values["device"]["forwardnoanswerenable"] = false
values["device"]["mac"] = ""
values["device"]["musiconhold"] = ""
values["device"]["pcportenable"] = true
values["device"]["registrar"] = ""
values["device"]["sntpserver"] = ""
values["device"]["template"] = "/etc/provisioning/templates/snom-template.lua"
values["device"]["timezone"] = ""
values["device"]["urldialingenable"] = false
values["regX"]["callerid"] = ""
values["regX"]["extension"] = ""
values["regX"]["password"] = ""
values["services"]["callhistoryenable"] = true
values["services"]["missedcallhistoryenable"] = true
values["services"]["receivedcallhistoryenable"] = true
values["services"]["callwaitingenable"] = true
values["services"]["forwarding"] = true
values["services"]["hotlinedestination"] = ""
values["services"]["hotlineenable"] = false
values["services"]["mailbox"] = ""
-- Unimplemented -- values["services"]["mailcallback"] = ""
-- Unimplemented -- values["services"]["speeddialenable"] = true
--]]

local function onoff ( bool )
	if bool then
		return "on"
	else
		return "off"
	end
end

local function xml_elem(elem,value,permissions,i)
	local output = {"<", elem, ' perm="', permissions, '"'}
	if i then output[#output+1] = ' idx="'..i..'"' end
	output[#output+1] = ">"
	if type(value) == "boolean" then
		output[#output+1] = onoff(value)
	elseif value then
		output[#output+1] = value
	end
	output[#output+1] = "</"..elem..">"
	io.write(table.concat(output).."\n")
end
%>

<?xml version="1.0" encoding="utf-8"?>
<settings>
  <phone-settings>
<%
	xml_elem('admin_mode_password', values.device.adminpassword, 'R')
	xml_elem('http_pass', values.device.adminpassword, 'R')
    	xml_elem('http_user', 'admin', 'R')
	xml_elem('eth_pc', values.device.pcportenable, 'R')
	xml_elem('ntp_server', values.device.sntpserver, 'R')
	xml_elem('block_url_dialing', not values.device.urldialingenable, 'R')
	xml_elem('call_waiting', values.services.callwaitingenable, 'R')

if values.services and values.services.mailbox and values.services.mailbox ~= "" then
	xml_elem('user_mailbox', 'sip:'..values.services.mailbox, 'R')
else
	xml_elem('user_mailbox', '', 'R')
end

if values.services and values.services.forwarding then
	-- SNOMs do not support per-line forwarding
	xml_elem('redirect_allways', values.device.forwardallenable, 'RW')
	xml_elem('redirect_number', values.device.forwardall, 'RW')
	xml_elem('redirect_on_busy', values.device.forwardbusyenable, 'RW')
	xml_elem('redirect_busy_number', values.device.forwardbusy, 'RW')
	xml_elem('redirect_on_timeout', values.device.forwardnoanswerenable, 'RW')
	xml_elem('redirect_time_number', values.device.forwardnoanswer, 'RW')
else
	xml_elem('redirect_allways', false, 'RW')
	xml_elem('redirect_on_busy', false, 'RW')
	xml_elem('redirect_on_timeout', false, 'RW')
end

if values.services and values.services.hotlineenable and values.services.hotlinedestination and values.services.hotlinedestination ~= "" then
	xml_elem('action_offhook_url', 'http://127.0.0.1/command.htm?number='..values.services.hotlinedestination, 'R')
else
	xml_elem('action_offhook_url', '', 'R')
end

-- Handle Time Zone parameter
--   See http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html
--     TZ variable, with no leading colon
--   Syntax: stdoffset[dst[offset][,start[/time],end[/time]]]
--   Examples: 'GMT0' 'EST5EDT,M3.2.0,M11.1.0' '<GMT+5>5'
--  Parse time zone variable
posixtz = require('posixtz')
local tz = posixtz.parse(values.device.timezone)
if tz then
	-- convert POSIX sign (W of GMT is '+') to Snom (E of GMT is '+')
	xml_elem('utc_offset', -1 * tz.offset.total, 'R')

	local function dstrule(t)
		local date, time

		if t.day then
			date = string.format('%02i', t.day)..'.'..string.format('%02i', t.month)
		else
			-- POSIX weekday is between 0 (Sun) and 6 (Sat)
			-- Snom weekday is between 1=Mon, 7=Sun
			if tonumber(t.weekday) == 0 then
				t.weekday = 7
			end
			date = string.format('%02i', t.month)..'.'..string.format('%02i', t.week)..'.'..string.format('%02i', t.weekday)
		end


		-- Handle explicit hour for DST change (posixtz only handles hour for now)
		if t.hour then
			time = string.format('%02i', t.hour)..':00:00'
		else
			time = '2:00:00'
		end

		return date..' '..time
	end

	if tz.dst then
		xml_elem('dst', '3600 '..dstrule(tz.dst.start)..' '..dstrule(tz.dst.stop), 'R')
	else
		xml_elem('dst', '', 'R')
	end
else
	xml_elem('utc_offset', 0, 'R')
	xml_elem('dst', '', 'R')
end

for pg, pg_t in pairs(values) do
	-- Is it of the form regX ?
	local num = string.match(pg, 'reg(%d+)')
	if num then
		if pg_t.extension ~= "" then
			xml_elem('user_active', 'on', 'R', num )
			xml_elem('record_dialed_calls', values.services.callhistoryenable, 'R', num)
			xml_elem('record_missed_calls', values.services.missedcallhistoryenable, 'R', num)
			xml_elem('record_received_calls', values.services.receivedcallhistoryenable, 'R', num)
			xml_elem('user_host', values.device.registrar, 'R', num )
			if values.device.musiconhold and values.device.musiconhold ~= "" then
				xml_elem('user_moh', 'sip:'..values.device.musiconhold, 'R', num)
			end
			xml_elem('user_name', pg_t.extension, 'R', num)
			xml_elem('user_outbound', values.device.registrar, 'R', num)
			xml_elem('user_pass', pg_t.password, 'R', num)
			-- Caller ID string
			if string.len(pg_t.callerid) == 0 then
				xml_elem( 'user_realname', pg_t.extension,'R', num)
			else
				xml_elem( 'user_realname', pg_t.callerid,'R', num)
			end
		else
			xml_elem('user_active', 'off', 'R', num )
		end
	end
end
%>
  </phone-settings>
</settings>
