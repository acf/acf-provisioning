<?xml version="1.0" standalone="yes"?>

<%
-- CyberData Intercom Configuration File Template
local values = ...

xml = require("LuaXml")
fs = require("acf.fs")

local FirmwareFile = "/var/www/provisioning/htdocs/000000cd.xml"
local InitFile = "/var/www/provisioning/htdocs/CyberData/initial.xml"

local function findSection(xmlobj, name)
	-- Cannot use xmlobj:find() because we don't want recursion
	for i,s in ipairs(xmlobj) do
		if s:tag() == name then
			return s
		end
	end
	return null
end

local function findOrAppendSection(xmlobj, name)
	return findSection(xmlobj, name) or xmlobj:append(name)
end

local function setValue(xmlobj, name, value)
	if value then
		findOrAppendSection(xmlobj, name)[1] = value
	end
end

function mergeXML(xmlobj, xmlobj2)
	-- [0] is the name
	-- [1] is the value or a table for section
	if type(xmlobj2[1]) == "string" then
		xmlobj[0] = xmlobj2[0]
		xmlobj[1] = xmlobj2[1]
	else
		for i,v in ipairs(xmlobj2) do
			mergeXML(findOrAppendSection(xmlobj, xmlobj2[i][0]), xmlobj2[i])
		end
	end
	return xmlobj
end

local function yesno(xmlobj, name, value)
	if value ~= nil then
		if (value == false) then
		value = "No"
		elseif (value == true) then
		value = "Yes"
		end
		setValue(xmlobj, name, value)
	end
end

local function loadConfig(file)
	local cfg
	local res, err = pcall(function()
		--cfg = xml.load(file)

		-- Hack to allow download of config files with firmware URL, replacing $SERV with SERVER_NAME
		local contents = fs.read_file(file) or ""
		if ENV and ENV.SERVER_NAME then
			contents = string.gsub(contents, "%$SERV", ENV.SERVER_NAME)
		end
		cfg = xml.eval(contents)
	end)
	return cfg or xml.new("specific")
end

-- Load the initial config
local cfg = loadConfig(FirmwareFile)
local init_cfg = loadConfig(InitFile)

cfg = mergeXML(cfg, init_cfg)

-- <IPSettings>

-- <ClockSettings>

if values.device then
	local clocksettings = findOrAppendSection(cfg, "ClockSettings")
	setValue(clocksettings, "NTPServer", values.device.sntpserver)
	setValue(clocksettings, "NTPTimezone", values.device.timezone)
end

-- <SIPSettings>

if values.reg1 then
	local sipsettings = findOrAppendSection(cfg, "SIPSettings")
	if values.device then
		setValue(sipsettings, "SIPServer", values.device.registrar)
	end
	setValue(sipsettings, "SIPUserID", values.reg1.extension)
	setValue(sipsettings, "SIPAuthID", values.reg1.extension)
	setValue(sipsettings, "SIPAuthPassword", values.reg1.password)
	-- These parameters are used by CyberData Intercom
	if values.services and values.services.hotlineenable and values.services.hotlinedestination then
		setValue(sipsettings, "DialoutExtension0", values.services.hotlinedestination)
	else
		setValue(sipsettings, "DialoutExtension0", "")
	end
	setValue(sipsettings, "DialoutID0", values.reg1.callerid)
end

-- <DeviceSettings>

if values.device then
	local DeviceSettings = findOrAppendSection(cfg, "DeviceSettings")
	-- These parameters are used by CyberData Intercom and CyberData Paging Zone Controller
	setValue(DeviceSettings, "AdminPassword", values.device.adminpassword)
	-- These parameters are used by CyberData Intercom
	setValue(DeviceSettings, "SpeakerVolume", values.device.cyberspeakervolume)
	setValue(DeviceSettings, "RingVolume", values.device.cyberringvolume)
	setValue(DeviceSettings, "MicGain", values.device.cybermicgain)
	yesno (DeviceSettings, "ActivateRelayWithDTMF", values.device.activaterelaywithdtmf)
	setValue(DeviceSettings, "DTMFActivationCode", values.device.dtmfactivationcode)
	setValue(DeviceSettings, "DTMFActivationDuration", values.device.dtmfactivationduration)
	yesno (DeviceSettings, "ActivateRelayDuringRing", values.device.activaterelayduringring)
	yesno (DeviceSettings, "ActivateRelayDuringCall", values.device.activaterelayduringcall)
	yesno (DeviceSettings, "AutoAnswerIncomingCalls", values.device.autoanswerincomingcalls)
	-- These parameters are used by CyberData Paging Zone Controller
	yesno (DeviceSettings, "BypassDTMF", values.device.bypassdtmf)
	yesno (DeviceSettings, "BeepOnInitialization", values.device.beeponinitialization)
	yesno (DeviceSettings, "BeepBeforePage", values.device.beepbeforepage)
end

-- <ZoneSettings>

-- These parameters are used by CyberData Paging Zone Controller
if values.device then
	local ZoneSettings = findOrAppendSection(cfg, "ZoneSettings")
	yesno(ZoneSettings, "BypassDTMF", values.device.bypassdtmf)
end

-- Loop through Parameter Groups looking for 'zone' params
for pg, pg_t in pairs(values) do
	-- Is it of the form zoneXX ?
	local num = string.match(pg, 'zone(%d%d)')
	if num then
		local ZoneSettings = findOrAppendSection(cfg, "ZoneSettings")
		yesno(ZoneSettings, 'Zone'..num..'Port1', pg_t.pagingport1)
		yesno(ZoneSettings, 'Zone'..num..'Port2', pg_t.pagingport2)
		yesno(ZoneSettings, 'Zone'..num..'Port3', pg_t.pagingport3)
		yesno(ZoneSettings, 'Zone'..num..'Port4', pg_t.pagingport4)
	end
end

-- <ButtonSettings>

-- These parameters are used by CyberData Intercom
if values.device then
	local ButtonSettings = findOrAppendSection(cfg, "ButtonSettings")
	yesno (ButtonSettings, "PlayToneWhileRelayActive", values.device.playtonewhilerelayactive)
end

-- <SensorSettings>

-- These parameters are used by CyberData Intercom
if values.device then
	local SensorSettings = findOrAppendSection(cfg, "SensorSettings")
	yesno (SensorSettings, "SensorNormallyClosed", values.device.sensornormallyclosed)
	setValue (SensorSettings, "SensorTimeout", values.device.sensortimeout)
	yesno (SensorSettings, "SensorFlashLED", values.device.sensorflashled)
	yesno (SensorSettings, "SensorActivateRelay", values.device.sensoractivaterelay)
	yesno (SensorSettings, "SensorPlayLocally", values.device.sensorplaylocally)
	yesno (SensorSettings, "SensorCall", values.device.sensorcall)
	setValue (SensorSettings, "SensorDialOutExtension", values.device.sensordialoutextension)
	setValue (SensorSettings, "SensorDialOutID", values.device.sensordialoutid)
	yesno (SensorSettings, "SensorRepeat", values.device.sensorrepeat)
	yesno (SensorSettings, "SensorPlayRemotely", values.device.sensorplayremotely)
end

-- cfg:save("TEST-cyberdata.xml")

print(cfg)
%>
