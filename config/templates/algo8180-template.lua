<%
-- Algo 8180 SIP Audio Alerter Configuration File Template
local values = ...

local function yesno ( bool )
	if bool then
		return "Yes"
	else
		return "No"
	end
end

local function set_elem(elem,value,i)
	local e = elem
	if i then e = elem.."_"..tostring(i).."_" end
	local output = {e.." = "}
	if type(value) == "boolean" then
		output[#output+1] = yesno(value)
	elseif value then
		output[#output+1] = value
	end
	io.write(table.concat(output).."\n")
end
%>

<%
-- Include the static values from init.cfg. This can be overridden by the following values from the database
local f = io.open("/var/www/provisioning/htdocs/Algo/init.cfg", "r")
if f then
	for line in f:lines() do
		print(line)
	end
	f:close()
end
%>

<%
set_elem("admin.pwd", values.device.adminpassword)
set_elem("net.time", values.device.sntpserver)
set_elem("sip.proxy", values.device.registrar)
set_elem("audio.ring.vol", values.device.algoringvol)
set_elem("audio.page.vol", values.device.algopagevol)
set_elem("audio.spk.mode", values.device.algospkmode)
for pg, pg_t in pairs(values) do
	-- Is it of the form regX ?
	local num = string.match(pg, 'reg(%d+)')
	if tostring(num) < "6" then
		if pg_t.extension ~= "" then
			set_elem("sip.alert"..num..".auth", pg_t.extension)
			set_elem("sip.alert"..num..".pwd", pg_t.password)
			set_elem("sip.alert"..num..".user", pg_t.extension)
			if tostring(num) == "1" then
				set_elem("audio.ring.tone", pg_t.algoringtone)
			else
				set_elem("audio.ring.tone"..num.."", pg_t.algoringtone)
			end
		end
	elseif num == "6" then
		if pg_t.extension ~= "" then
			set_elem("sip.u1.auth", pg_t.extension)
			set_elem("sip.u1.pwd", pg_t.password)
			set_elem("sip.u1.user", pg_t.extension)
			set_elem("audio.page.tone", pg_t.algopagetone)
		end
	end
end
%>
