<?xml version="1.0" standalone="yes"?>

<%
-- Grandstream Configuration File Template
local values = ...

xml = require("LuaXml")

local init_cfg = xml.load("/var/www/provisioning/htdocs/Grandstream/init.cfg")

-- <config>
init_cfg[1]:append("P2")[1] = values.device.adminpassword
init_cfg[1]:append("P30")[1] = values.device.sntpserver
init_cfg[1]:append("P246")[1] = values.device.timezone

for pg, pg_t in pairs(values) do
	-- Is it of the form regX ?
	local num = string.match(pg, 'reg(%d+)')
	if num == "1" then
		if pg_t.extension ~= "" then
			init_cfg[1]:append("P47")[1] = values.device.registrar
			init_cfg[1]:append("P35")[1] = pg_t.extension
			init_cfg[1]:append("P36")[1] = pg_t.extension
			init_cfg[1]:append("P34")[1] = pg_t.password
			init_cfg[1]:append("P3")[1] = pg_t.callerid
		end
	elseif num == "2" then
		if pg_t.extension ~= "" then
			init_cfg[1]:append("P747")[1] = values.device.registrar
			init_cfg[1]:append("P735")[1] = pg_t.extension
			init_cfg[1]:append("P736")[1] = pg_t.extension
			init_cfg[1]:append("P734")[1] = pg_t.password
			init_cfg[1]:append("P703")[1] = pg_t.callerid
		end
	end
end

print(init_cfg)
%>
