<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
        $(document).ready(function() {
                $("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		$(".deleteparam").click(function(){ return confirm("Are you sure you want to delete this parameter?")});
        });
</script>

<% htmlviewfunctions.displaycommandresults({"deleteparam", "editparam"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createparam"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Name</th>
		<th>Label</th>
		<th>Default</th>
		<th>Sequence</th>
	</tr>
</thead><tbody>
<% local param_id = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for k,v in ipairs( view.value ) do %>
	<tr>
		<td>
		<% param_id.value = v.param_id %>
		<% if viewlibrary.check_permission("editparam") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={param_id=param_id, redir=redir}, label="", option="Edit", action="editparam"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("deleteparam") then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={param_id=param_id}, label="", option="Delete", action="deleteparam", class="deleteparam"}), page_info, -1) %>
		<% end %>
		</td>
                <td><%= html.html_escape(v.name) %></td>
                <td><%= html.html_escape(v.label) %></td>
                <td><%= html.html_escape(v.value) %></td>
                <td><%= html.html_escape(v.seq) %></td>
	</tr>
<% end %>
</tbody>
</table>

<% if view.errtxt then %>
<p class="error"><%= html.html_escape(view.errtxt) %></p>
<% end %>
<% if #view.value == 0 then %>
<p>No parameters found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createparam") then
        viewlibrary.dispatch_component("createparam")
end %>
