<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.widgets.js"><\/script>');
		document.write('<link href="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/jquery.tablesorter.pager.css" rel="stylesheet">');
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/widgets/widget-pager.js"><\/script>');
	}
</script>

<script type="text/javascript">
        $(document).ready(function() {
                $("#list").tablesorter({headers: {0:{sorter:'digit'}}, widgets: ['zebra', 'filter', 'pager'], widgetOptions: {
			// Filtering is handled by the server
			filter_serversideFiltering: true,

			// We can put the page number and size here, filtering and sorting handled by pager_customAjaxUrl
			pager_ajaxUrl : '<%= html.html_escape(page_info.script .. page_info.orig_action) %>?viewtype=json&page={page+1}&pagesize={size}',

			// Modify the url after all processing has been applied to handle filtering and sorting
			pager_customAjaxUrl: function(table, url) {
				var columns = ["logdatetime", "msgtext", "userid"];
				var directions = ["asc", "desc"];
				for (var s=0; s<table.config.sortList.length; s++) {
					// 0=column number, 1=direction(0 is asc)
					if ((table.config.sortList[s][0] < columns.length) && (table.config.sortList[s][1] < directions.length)) {
						url += "&orderby."+(s+1)+".column="+columns[table.config.sortList[s][0]]+"&orderby."+(s+1)+".direction="+directions[table.config.sortList[s][1]]
					}
				}
				for (var f=0; f<table.config.pager.currentFilters.length; f++) {
					var filter = table.config.pager.currentFilters[f];
					if (filter.trim()) {
						url += "&filter."+columns[f]+"="+encodeURIComponent(filter.trim());
					}
				}
				return url;
			},

			// process ajax so that the following information is returned:
			// [ total_rows (number), rows (array of arrays), headers (array; optional) ]
			pager_ajaxProcessing: function(data){
				if (data && data.value && data.value.result) {
					rows = [];
					for ( r=0; r<data.value.result.value.length; r++) {
						row=[];
						row[0] = '<span class="hide">'+Date.UTC(data.value.result.value[r].logdatetime)+'</span>'+data.value.result.value[r].logdatetime;
						row[1] = data.value.result.value[r].msgtext;
						row[2] = data.value.result.value[r].userid;
						rows.push(row);
					}
					return [ parseInt(data.value.rowcount.value), rows];
				}
			}
		}});
        });
</script>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th class="filter-false">Timestamp</th>
		<th>Message</th>
		<th>User ID</th>
	</tr>
</thead><tbody>
</tbody>
</table>

<div id="pager" class="pager">
	<form>
		Page: <select class="gotoPage"></select>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/first.png" class="first"/>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/prev.png" class="prev"/>
		<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/next.png" class="next"/>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/last.png" class="last"/>
		<select class="pagesize">
			<option selected="selected" value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option value="40">40</option>
		</select>
	</form>
</div>

<% htmlviewfunctions.displaysectionend(header_level) %>
