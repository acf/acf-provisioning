<% local data, viewlibrary, page_info = ... %>

<%
if not data.errtxt and data.value.values and data.value.values.value.device and data.value.values.value.device.template then
	local func = haserl.loadfile(data.value.values.value.device.template)
	func(data.value.values.value)
end
%>
