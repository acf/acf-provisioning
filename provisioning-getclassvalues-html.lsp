<% local form, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<%
if form.value.values then
	local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Class"}), page_info)
	htmlviewfunctions.displayinfo(form)
	htmlviewfunctions.displayitem(form.value.class_id)
	htmlviewfunctions.displayitem(form.value.label)
	local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Parameter Values"}), page_info, htmlviewfunctions.incrementheader(header_level))
	val = require("session").serialize("values", form.value.values.value)
	val = string.gsub(val, "[^\n]*%{%}\n", "")
	print("<pre>"..val.."</pre>")
	htmlviewfunctions.displaysectionend(header_level2)
	htmlviewfunctions.displaysectionend(header_level)
	form.value.values = nil
end
%>

<%
	form.value.values = nil
	form.value.label = nil
	htmlviewfunctions.displayitem(form, page_info)
%>
