<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
format = require("acf.format")
html = require("acf.html")
%>

<%
-- Function to mark params that overridden the default set in a param group
function markoverride(c)
	if c.type == "group" then
		for n,v in pairs(c.value) do
			markoverride(v)
		end
	elseif (c.groupdefault == false or c.groupdefault) and c.value ~= c.groupdefault then
		if c.class then
			c.class = c.class .." groupdefaultoverride"
		else
			c.class = "groupdefaultoverride"
		end
	elseif (c.default == false or c.default) and c.value ~= c.default then
		if c.class then
			c.class = c.class .." defaultoverride"
		else
			c.class = "defaultoverride"
		end
	end
end

if string.find(page_info.orig_action, format.escapemagiccharacters(page_info.action).."$") and form.value.device_id and form.value.device_id.value ~= "" then
	page_info.orig_action = page_info.orig_action.."?device_id="..form.value.device_id.value
end
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
<% -- Since we're including deletedevice as a component, we break the automatic redirect
	if session.deletedeviceresult and not session.deletedeviceresult.errtxt and viewlibrary.check_permission("listdevices") then
		-- Use JavaScript to redirect to the listdevices page
%>
		window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/listdevices");
<%
	end
%>

	function showsection() {
		$("#"+$(this).attr("class")).toggle();
		if ($(this).next().next("a").length == 1) {
			$(this).next().next().toggle();
		}
		$(this).remove();
	}

	function hideunuseddivs() {
		var previoussection = "";
		var previousnumber = 0;
		var previouslabel = "";
		var handledfirst = false;
		$("div").filter(function(){
			var id = $(this).attr("id");
			// We're looking for an id that starts with section_
			if ((id != null) && (id.indexOf("section_") === 0)) {
				return true;
			}
			return false;
		}).each(function(i){
			var id = $(this).attr("id");
			var currentnumber = id.match(/\d+$/);
			var currentsection
			var currentlabel
			if (currentnumber != null) {
				var len = id.length;
				var numlen = currentnumber[0].length;
				currentsection = id.substr(8, id.length-8-currentnumber[0].length);
				currentnumber = currentnumber[0].toString();
				currentlabel = $(this).children().first().text().replace(/\d+/, "");
			} else {
				currentsection = id.substr(8);
				currentnumber = 0;
			}

			if ((currentsection == previoussection) && (currentnumber != previousnumber) && (currentlabel == previouslabel)) {
				// Found a section
				<% if page_info.action ~= "viewdeviceparams" and viewlibrary.check_permission("deletedeviceparamgroup") then %>
				if (!handledfirst) {
					// Add a delete button to previous section
					$("<a href='<%= page_info.script..page_info.prefix..page_info.controller.."/deletedeviceparamgroup?device_id="..html.html_escape(form.value.device_id.value) %>&group="+previoussection+previousnumber+"&redir=<%= page_info.script..html.url_encode(page_info.orig_action) %><% if form.value.redir then %><%= html.url_encode("&redir="..form.value.redir.value)%><% end %>&submit=true' class='deletedeviceparamgroup'> Delete</a>").insertAfter($(this).prevAll("div").first().children().first());
					handledfirst = true;
				}
				$("<a href='<%= page_info.script..page_info.prefix..page_info.controller.."/deletedeviceparamgroup?device_id="..html.html_escape(form.value.device_id.value) %>&group="+currentsection+currentnumber+"&redir=<%= page_info.script..html.url_encode(page_info.orig_action) %><% if form.value.redir then %><%= html.url_encode("&redir="..form.value.redir.value)%><% end %>&submit=true' class='deletedeviceparamgroup'> Delete</a>").insertAfter($(this).children().first());
				<% end %>
				if ((0 == $(this).find(".defaultoverride").length) && (0 == $(this).find(".groupdefaultoverride").length)) {
					// Hide this section
					$(this).toggle();
					<% if page_info.action ~= "viewdeviceparams" then %>
					$('<a href="javascript:;" class="'+id+'"><img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>tango/16x16/actions/list-add.png"> '+$(this).children().first().text()+'</a>').insertAfter($(this)).click(showsection);
					if ($(this).prev("a").length == 1) {
						// If the previous sibling is also an Add link, hide this one
						$(this).next().toggle();
					}
					<% end %>
				}
			} else {
				handledfirst = false;
			}

			previoussection = currentsection;
			previousnumber = currentnumber;
			previouslabel = currentlabel;
		});

	}

        $(document).ready(function() {
		$(".deletedevice").click(function(){ return confirm("Are you sure you want to delete this device?")});
		$(".groupdefaultoverride").siblings().select("contains('Default:')").addClass("error");
		hideunuseddivs();
		$(".deletedeviceparamgroup").click(function(){ return confirm("Are you sure you want to delete "+$(this).prev().text()+"? Unsaved changes will be lost.")});
        });
</script>

<% htmlviewfunctions.displaycommandresults({"deletedeviceparamgroup"}, session) %>

<% if form and form.value and form.value.device_id and form.value.device_id.value ~= "" then %>
	<% if viewlibrary.check_permission("editdevice") then %>
		 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/editdevice?device_id="..html.html_escape(form.value.device_id.value) %>'><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/categories/applications-system.png' title="Edit Class of Service"></img> <big>Edit Class of Service </big></a>
	<% end %>
	<% if viewlibrary.check_permission("deletedevice") then %>
		 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/deletedevice?submit=true&device_id="..html.html_escape(form.value.device_id.value) %>' class="deletedevice"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/emblems/emblem-unreadable.png' title="Delete this Device"></img> <big>Delete this Device </big></a>
	<% end %>
<% end %>

<%
	form.value.device_id.readonly = "true"
	if not form.errtxt and form.value.groupdefaultoverride.value then
		form.errtxt = "Warning: Class defaults have been overridden for this device"
	end
	form.value.groupdefaultoverride = nil

	-- Mark the parameters where the group default is overridden
	for n,v in pairs(form.value) do
		markoverride(v)
	end

	htmlviewfunctions.displayitem(form, page_info)
%>
