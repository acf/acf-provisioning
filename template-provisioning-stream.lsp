<% local view, viewlibrary, page_info, session = ... %>
<%
local viewtable = view
if view.value.result then
	-- Special case for provisioning result
	viewtable.option = "text/csv"
	viewtable.label = page_info.action..os.date("_%Y%m%d_%H%M%S.csv")
elseif view.type == "group" or view.type == "form" then
	viewtable = nil
	for name,value in pairs(view.value) do
		if value.type == "raw" then
			viewtable = value
			break
		end
	end
	if not viewtable then
		viewtable = cfe({label="error", value=view.errtxt or "Invalid stream output"})
	end
	-- Special case for provisioning
	viewtable.label = page_info.action..os.date("_%Y%m%d_%H%M%S.csv")
end
%>
Status: 200 OK
Content-Type: <% print(viewtable.option or "application/octet-stream") %>
<% if viewtable.length then %>
Content-Length: <%= viewtable.length %>
<% io.write("\n") %>
<% end %>
<% if viewtable.label ~= "" then %>
Content-Disposition: attachment; filename="<%= viewtable.label %>"
<% end %>
<% io.write("\n") %>
<% page_info.viewfunc(viewtable, viewlibrary, page_info, session) %>
