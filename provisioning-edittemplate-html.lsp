<% local form, viewlibrary, page_info = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<% htmlviewfunctions.displayformstart(form, page_info) %>
<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Template Details"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<%
if page_info.action == "edittemplate" then
	form.value.filename.readonly = "true"
end
htmlviewfunctions.displayformitem(form.value.filename, "filename")
htmlviewfunctions.displayformitem(form.value.label, "label")
htmlviewfunctions.displayformitem(form.value.seq, "seq")
%>
<% htmlviewfunctions.displaysectionend(header_level2) %>

<% htmlviewfunctions.displaysectionstart(cfe({label="File Content"}), page_info, header_level2) %>
<textarea name="filecontent">
<%= html.html_escape(form.value.filecontent.value) %>
</textarea>
<% htmlviewfunctions.displayinfo(form.value.filecontent) %>

<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displayformend(form) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
