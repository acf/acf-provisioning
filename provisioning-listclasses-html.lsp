<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
<% -- Since we're including a submission of getclassvalues, we need to overcome the automatic redirect
	if session.getclassvaluesresult then
		-- Use JavaScript to redirect to the getclassvalues page
%>
		window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/getclassvalues");
<%
	end
%>
        $(document).ready(function() {
                $("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		$(".deleteclass").click(function(){ return confirm("Are you sure you want to delete this class?")});
        });
</script>

<% htmlviewfunctions.displaycommandresults({"deleteclass", "editclass"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createclass"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Class Group</th>
		<th>Label</th>
		<th>Sequence</th>
	</tr>
</thead><tbody>
<% local class_id = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for k,v in ipairs( view.value ) do %>
	<tr>
		<td>
		<% class_id.value = v.class_id %>
		<% if viewlibrary.check_permission("editclass") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={class_id=class_id, redir=redir}, label="", option="Edit", action="editclass"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("getclassvalues") then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={class_id=class_id}, label="", option="View", action="getclassvalues"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("deleteclass") then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={class_id=class_id}, label="", option="Delete", action="deleteclass", class="deleteclass"}), page_info, -1) %>
		<% end %>
		</td>
                <td><%= html.html_escape(v.group) %></td>
                <td><%= html.html_escape(v.label) %></td>
                <td><%= html.html_escape(v.seq) %></td>
	</tr>
<% end %>
</tbody>
</table>

<% if view.errtxt then %>
<p class="error"><%= html.html_escape(view.errtxt) %></p>
<% end %>
<% if #view.value == 0 then %>
<p>No classes found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createclass") then
        viewlibrary.dispatch_component("createclass")
end %>
