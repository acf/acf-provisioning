<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(function(){
		$('input, select').first().focus();
		$('input, select').keydown(function(e){
			if (e.keyCode == 13) {
				$(this).parents('form').find('input.submit').click();
				return false;
			}
		});
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		$(".deletedevice").click(function(){ return confirm("Are you sure you want to delete this device?")});
		$("#display").attr('size', $("#display").find('option').length/10+4);
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletedevice", "editdevicecustom", "editdevice", "editdeviceparams", "overridedeviceparams"}, session) %>

<%
-- Display the list of devices in the result
if form.value.result then
	local display = {}
	if form.value.display then
		display = form.value.display.value
	else
		-- Determine all of the columns
		local tmp = {}
		for k,v in ipairs( form.value.result.value ) do
			for g,c in pairs(v) do
				if not tmp[g] then tmp[g] = true end
			end
		end
		for n in pairs(tmp) do
			if n ~= "device_id" then
				display[#display+1] = n
			end
		end
		table.sort(display)
	end

	local header_level = htmlviewfunctions.displaysectionstart(form.value.result, page_info)
%>
	<table id="list" class="tablesorter"><thead>
		<tr>
			<th>Action</th>
			<th>Device ID</th>
		<% for i,n in ipairs(display) do %>
			<th><%= html.html_escape(string.gsub(n, "^.", string.upper)) %>
		<% end %>
		</tr>
	</thead><tbody>
	<% local device_id = cfe({ type="hidden", value="" }) %>
	<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
	<% for k,v in ipairs( form.value.result.value ) do %>
		<tr>
			<td>
			<% device_id.value = v.device_id %>
			<% if viewlibrary.check_permission("editdevicecustom") then %>
				<% htmlviewfunctions.displayitem(cfe({type="link", value={device_id=device_id, redir=redir}, label="", option="Edit", action="editdevicecustom"}), page_info, -1) %>
			<% else %>
				<% if viewlibrary.check_permission("editdevice") then %>
					<% htmlviewfunctions.displayitem(cfe({type="link", value={device_id=device_id, redir=redir}, label="", option="Edit", action="editdevice"}), page_info, -1) %>
				<% end %>
				<% if viewlibrary.check_permission("overridedeviceparams") then %>
					<% htmlviewfunctions.displayitem(cfe({type="link", value={device_id=device_id, redir=redir}, label="", option="Params", action="overridedeviceparams"}), page_info, -1) %>
				<% elseif viewlibrary.check_permission("editdeviceparams") then %>
					<% htmlviewfunctions.displayitem(cfe({type="link", value={device_id=device_id, redir=redir}, label="", option="Params", action="editdeviceparams"}), page_info, -1) %>
				<% elseif viewlibrary.check_permission("viewdeviceparams") then %>
					<% htmlviewfunctions.displayitem(cfe({type="link", value={device_id=device_id}, label="", option="Details", action="viewdeviceparams"}), page_info, -1) %>
				<% end %>
			<% end %>
			<% if viewlibrary.check_permission("getdevicevalues") then %>
				<% htmlviewfunctions.displayitem(cfe({type="form", value={value=device_id, id=cfe({type="hidden", value="device_id"}), viewtype=cfe({type="hidden", value="templated"})}, label="", option="View", action="getdevicevalues"}), page_info, -1) %>
			<% end %>
			<% if viewlibrary.check_permission("deletedevice") then %>
				<% htmlviewfunctions.displayitem(cfe({type="form", value={device_id=device_id}, label="", option="Delete", action="deletedevice", class="deletedevice"}), page_info, -1) %>
			<% end %>
			</td>
			<td><%= html.html_escape(v.device_id) %></td>
		<% for i,n in ipairs(display) do %>
			<td><%= html.html_escape(v[n]) %></td>
		<% end %>
		</tr>
	<% end %>
	</tbody>
	</table>

	<% if form.value.result.errtxt then %>
	<p class="error"><%= html.html_escape(form.value.result.errtxt) %></p>
	<% end %>
	<% if #form.value.result.value == 0 then %>
	<p>No devices found</p>
	<% else %>
		<% local action = "searchdevices"
		if page_info.action == "searchdevicedetails" then action = "searchdevicedetails" end
		htmlviewfunctions.displayitem(cfe({type="form", value={
			viewtype=cfe({ type="hidden", value="stream" }),
			id=cfe({ type="hidden", value=form.value.id.value }),
			comparison=cfe({ type="hidden", value=form.value.comparison.value }),
			value=cfe({ type="hidden", value=form.value.value.value }),
			display=cfe({ type="hidden", value=form.value.display.value }) },
			label="", option="Download", action=action}), page_info, -1)
		%>
	<% end %>
	<% htmlviewfunctions.displaysectionend(header_level) %>

<% end %>

<%
	form.value.result = nil
	htmlviewfunctions.displayitem(form, page_info)
%>
