local mymodule = {}

mymodule.default_action = "listdevices"

mymodule.listtemplates = function( self )
	return self.model.list_templates()
end

mymodule.edittemplate = function( self )
	return self.handle_form(self, self.model.get_template, self.model.update_template, self.clientdata, "Save", "Edit Template", "Template Saved")
end

mymodule.createtemplate = function( self )
	return self.handle_form(self, self.model.get_template, self.model.create_template, self.clientdata, "Create", "Create Template", "Template Created")
end

mymodule.deletetemplate = function( self )
	return self.handle_form(self, self.model.get_delete_template, self.model.delete_template, self.clientdata, "Delete", "Delete Template", "Template Deleted")
end

mymodule.listclassgroups = function( self )
	return self.model.list_class_groups()
end

mymodule.editclassgroup = function( self )
	return self.handle_form(self, self.model.get_class_group, self.model.update_class_group, self.clientdata, "Save", "Edit Class Group", "Class Group Saved")
end

mymodule.createclassgroup = function( self )
	return self.handle_form(self, self.model.get_class_group, self.model.create_class_group, self.clientdata, "Create", "Create Class Group", "Parameter Class Created")
end

mymodule.deleteclassgroup = function( self )
	return self.handle_form(self, self.model.get_delete_class_group, self.model.delete_class_group, self.clientdata, "Delete", "Delete Class Group", "Class Group Deleted")
end

mymodule.listclasses = function( self )
	return self.model.list_classes()
end

mymodule.editclass = function( self )
	return self.handle_form(self, self.model.get_class, self.model.update_class, self.clientdata, "Save", "Edit Class", "Class Saved")
end

mymodule.createclass = function( self )
	return self.handle_form(self, self.model.get_class, self.model.create_class, self.clientdata, "Create", "Create Class", "Class Created")
end

mymodule.deleteclass = function( self )
	return self.handle_form(self, self.model.get_delete_class, self.model.delete_class, self.clientdata, "Delete", "Delete Class", "Class Deleted")
end

mymodule.getclassvalues = function( self )
	return self.handle_form(self, self.model.get_class_options, self.model.get_class_values, self.clientdata, "Fetch", "Get Device Values")
end

mymodule.listgroups = function( self )
	return self.model.list_groups()
end

mymodule.editgroup = function( self )
	return self.handle_form(self, self.model.get_group, self.model.update_group, self.clientdata, "Save", "Edit Parameter Group")
end

mymodule.creategroup = function( self )
	return self.handle_form(self, self.model.get_group, self.model.create_group, self.clientdata, "Create", "Create Parameter Group", "Parameter Group Created")
end

mymodule.deletegroup = function(self )
	return self.handle_form(self, self.model.get_delete_group, self.model.delete_group, self.clientdata, "Delete", "Delete Parameter Group", "Parameter Group Deleted")
end

mymodule.listparams = function( self )
	return self.model.list_params()
end

mymodule.editparam = function( self )
	return self.handle_form(self, self.model.get_param, self.model.update_param, self.clientdata, "Save", "Edit Parameter")
end

mymodule.createparam = function( self )
	return self.handle_form(self, self.model.get_param, self.model.create_param, self.clientdata, "Create", "Create Parameter", "Parameter Created")
end

mymodule.deleteparam = function( self )
	return self.handle_form(self, self.model.get_delete_param, self.model.delete_param, self.clientdata, "Delete", "Delete Parameter", "Parameter Deleted")
end

mymodule.listdevices = function( self )
	return self.model.list_devices(self, self.clientdata, true)
end

mymodule.detaildevices = function( self )
	return self.model.list_devices(self, self.clientdata)
end

mymodule.editdevice = function( self )
	return self.handle_form(self, self.model.get_existing_device, self.model.update_device, self.clientdata, "Save", "Edit Device", "Device Saved")
end

mymodule.editdevicecustom = mymodule.editdevice

mymodule.createdevice = function( self )
	return self.handle_form(self, self.model.get_new_device, self.model.create_device, self.clientdata, "Create", "Create Device")
end

mymodule.duplicatedevice = function( self )
	return self.handle_form(self, self.model.get_existing_device, self.model.create_device, self.clientdata, "Duplicate", "Duplicate Device", "Duplicate Device Created")
end

mymodule.deletedevice = function( self )
	return self.handle_form(self, self.model.get_delete_device, self.model.delete_device, self.clientdata, "Delete", "Delete Device", "Device Deleted")
end

mymodule.editdeviceparams = function( self )
	return self.handle_form(self, self.model.get_editable_device_params, self.model.set_editable_device_params, self.clientdata, "Save", "Edit Device Parameters", "Device Parameters Saved")
end

mymodule.overridedeviceparams = function( self )
	return self.handle_form(self, self.model.get_all_device_params, self.model.set_all_device_params, self.clientdata, "Save", "Override Device Parameters", "Device Parameters Saved")
end

mymodule.deletedeviceparamgroup = function( self )
	return self.handle_form(self, self.model.get_delete_device_param_group, self.model.delete_device_param_group, self.clientdata, "Delete", "Delete Device Parameter Group", "Device Parameter Group Deleted")
end

mymodule.viewdeviceparams = function( self )
	return self.model.get_editable_device_params(self, self.clientdata)
end

mymodule.getdevicevalues = function( self )
	return self.handle_form(self, self.model.get_fetch_options, self.model.fetch_device_values, self.clientdata, "Search", "Get Device Values")
end

mymodule.searchdevices = function( self )
	return self.handle_form(self, function(self, clientdata) return self.model.get_search_options(self, clientdata, true) end, function(self, search, action) return self.model.search_device_values(self, search, action, true) end, self.clientdata, "Search", "Search Device Parameters")
end

mymodule.searchdevicedetails = function( self )
	return self.handle_form(self, self.model.get_search_options, self.model.search_device_values, self.clientdata, "Search", "Search Device Parameters")
end

mymodule.editoptions = function( self )
	return self.handle_form(self, self.model.get_param_options, self.model.set_param_options, self.clientdata, "Save", "Edit Parameter Options", "Parameter Options Saved")
end

mymodule.listfiles = function( self )
	return self.model.list_files()
end

mymodule.editfile = function( self )
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit Script File", "Script File Saved")
end

mymodule.dumpdatabase = function( self )
	return self.handle_form(self, self.model.get_database, self.model.dump_database, self.clientdata, "Submit", "Dump Provisioning Database")
end

mymodule.getfile = function( self )
	return self.model.get_file(self, self.clientdata)
end

mymodule.putfile = function( self )
	return self.model.put_file(self, self.clientdata)
end

mymodule.listrequests = function( self )
	return self.model.list_requests(self, self.clientdata)
end

mymodule.deleterequest = function( self )
	return self.handle_form(self, self.model.get_delete_request, self.model.delete_request, self.clientdata, "Delete", "Delete Request", "Request Deleted")
end

mymodule.createdevicefromrequest = function( self )
	return self.handle_form(self, self.model.get_request, self.model.create_from_request, self.clientdata, "Create", "Create Device", "Device Created")
end

mymodule.bulkcreatedevices = function( self )
	return self.handle_form(self, self.model.get_bulk_create_request, self.model.bulk_create_devices, self.clientdata, "Create", "Bulk Create Devices", "Devices Created")
end

mymodule.bulkdumpdevices = function( self )
	return self.handle_form(self, self.model.get_bulk_dump_request, self.model.bulk_dump_devices, self.clientdata, "Dump", "Bulk Dump Devices")
end

mymodule.bulkdumprawdevices = function( self )
	return self.handle_form(self, self.model.get_bulk_dump_request, self.model.bulk_dump_raw_devices, self.clientdata, "Dump", "Bulk Dump Devices")
end

mymodule.viewactivitylog = function( self )
	return self.model.getactivitylog(self, self.clientdata)
end

return mymodule
