<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
<% -- Since we want to redirect to edit a new device's parameters, we create an automatic redirect
	if page_info.action == "createdevice" and form.descr and not form.errtxt then
		-- Use JavaScript to redirect to the edit page
%>
		window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/<% if viewlibrary.check_permission("editdevicecustom") then io.write("editdevicecustom") elseif viewlibrary.check_permission("overridedeviceparams") then io.write("overridedeviceparams") elseif viewlibrary.check_permission("editdeviceparams") then io.write("editdeviceparams") else io.write("editdevice") end %>?device_id=<%= form.value.device_id.value %>&redir=<%= html.html_escape(page_info.orig_action) %>");
<%
	end
%>
<% -- Since we're including deletedevice as a component, we break the automatic redirect
	if session.deletedeviceresult and not session.deletedeviceresult.errtxt and viewlibrary.check_permission("listdevices") then
		-- Use JavaScript to redirect to the listdevices page
%>
		window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/listdevices");
<%
	end
%>

        $(document).ready(function() {
		$(".deletedevice").click(function(){ return confirm("Are you sure you want to delete this device?")});
        });
</script>

<% if form and form.value and form.value.device_id and form.value.device_id.value ~= "" then %>
	<% if viewlibrary.check_permission("overridedeviceparams") then %>
		 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/overridedeviceparams?device_id="..html.html_escape(form.value.device_id.value) %>'><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/document-properties.png' title="Edit Parameters"></img> <big>Edit Parameters </big></a>
	<% elseif viewlibrary.check_permission("editdeviceparams") then %>
		 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/editdeviceparams?device_id="..html.html_escape(form.value.device_id.value) %>'><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/document-properties.png' title="Edit Parameters"></img> <big>Edit Parameters </big></a>
	<% end %>
	<% if viewlibrary.check_permission("deletedevice") then %>
		 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/deletedevice?submit=true&device_id="..html.html_escape(form.value.device_id.value) %>' class="deletedevice"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/emblems/emblem-unreadable.png' title="Delete this Device"></img> <big>Delete this Device </big></a>
	<% end %>
<% end %>

<% htmlviewfunctions.displaycommandresults({"deletedevice", "editdeviceparams", "overridedeviceparams"}, session) %>

<%
	if page_info.action == "editdevice" then
		form.value.device_id.readonly = "true"
	elseif page_info.action == "duplicatedevice" then
		form.value.classes = nil
	else
		form.value.device_id.type = "hidden"
	end
	htmlviewfunctions.displayitem(form, page_info)
%>
