<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if view.value.device_id.value ~= "" then %>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	var paramchanged = false;
	var alreadysubmitted = false;

	function loadparamform() {
		<%
		local redir = session.provisioningeditdevicecustomredir or page_info.orig_action
		if view.value.redir then
			redir = view.value.redir.value
		end
		session.provisioningeditdevicecustomredir = redir

		local url = {}
		url[#url+1] = html.html_escape(page_info.script..page_info.prefix..page_info.controller).."/"
		if viewlibrary.check_permission("overridedeviceparams") then url[#url+1] = "overridedeviceparams"
		elseif viewlibrary.check_permission("editdeviceparams") then url[#url+1] = "editdeviceparams" end
		url[#url+1] = "?component=true&device_id="..html.html_escape(view.value.device_id.value)
		url[#url+1] = "&orig_action="..html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/"..page_info.action..html.url_encode("?device_id=")..html.html_escape(view.value.device_id.value))
		url[#url+1] = "&redir="..html.html_escape(html.url_encode(redir))
		%>
		var url = "<%= table.concat(url) %>";
		$.ajaxSetup({cache:false});
		$('#paramdiv').load(url, function(responseTxt, statusTxt, xhr) {
			paramchanged = false;
			$(this).find('a:contains("Edit Class of Service")').hide();
			$(this).find('a:contains("Delete this Device")').hide();
			$(this).find('input[name="device_id"]').parent().parent().hide();
			$(this).find('*').change(paramchangehandler);
		});
	}

	function classchangehandler(event) {
		if (alreadysubmitted || (paramchanged && ! confirm("Discard parameter changes?"))) {
			$(this).val($(this).data('current'));
			return false;
		}
		alreadysubmitted = true;
		$(this).data('current', $(this).val());
		$('#paramdiv').hide();
		$('#classdiv').find('input[name="submit"]').click();
	}

	function paramchangehandler() {
		paramchanged = true;
	}

        $(document).ready(function() {
		$('input[name="submit"]').parent().parent().hide();
		$('select').each(function(index) {
			$(this).data('current', $(this).val())
		});
		$('#classdiv').find('*').change(classchangehandler);
		loadparamform();
                $(".deletedevicefromclass").click(function(){ return confirm("Are you sure you want to delete this device?")});
        });
</script>
<% end %>

<div id="classdiv">
<%
if view.value.device_id.value ~= "" then
	if viewlibrary.check_permission("deletedevice") then %>
                 <a href='<%= page_info.script..page_info.prefix..page_info.controller.."/deletedevice?submit=true&device_id="..html.html_escape(view.value.device_id.value) %>' class="deletedevicefromclass"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/emblems/emblem-unreadable.png' title="Delete this Device"></img> <big>Delete this Device </big></a>
        <% end
else
	view.value.device_id.errtxt = "Invalid entry"
end
view.value.device_id.readonly = true
view.value.redir = null
htmlviewfunctions.displayitem(view, page_info)
%>
</div>

<div id="paramdiv"></div>
