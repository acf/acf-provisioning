<% local view, viewlibrary, page_info, session = ... %>

<%
	csv_escape = function(val)
		-- Replace " with "" and surround with "'s if contains " or ,
		local v,c = string.gsub(val or "", '"', '""')
		if c>0 or string.find(v, ",") then
			return '"'..v..'"'
		else
			return v
		end
	end

	if view.value.result then
		local display = {}
		local escapedvalues = {}
		if view.value.display then
			local founddeviceid = false
			for i,d in ipairs(view.value.display.value) do
				founddeviceid = founddeviceid or (d == "device_id")
				display[#display+1] = d
				escapedvalues[#escapedvalues+1] = csv_escape(d)
			end
			if not founddeviceid then
				-- Add device_id to beginning of list
				table.insert(display, 1, "device_id")
				table.insert(escapedvalues, 1, "device_id")
			end
		elseif #view.value.result.value > 0 then
			for name,value in pairs(view.value.result.value[1]) do
				display[#display+1] = name
				escapedvalues[#escapedvalues+1] = csv_escape(name)
			end
			-- Just use alphabetical order for the fields
			table.sort(display)
			table.sort(escapedvalues)
		end
		print(table.concat(escapedvalues, ","))
		for i,value in ipairs(view.value.result.value) do
			escapedvalues = {}
			for j,name in ipairs(display) do
				escapedvalues[#escapedvalues+1] = csv_escape(view.value.result.value[i][name])
			end
			print(table.concat(escapedvalues, ","))
		end
	else
		io.write(tostring(view.value))
	end
%>
