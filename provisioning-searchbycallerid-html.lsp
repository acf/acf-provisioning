<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(function(){
		$("input[name='value']").focus();
		$('input').keydown(function(e){
			if (e.keyCode == 13) {
				$(this).parents('form').find('input.submit').click();
				return false;
			}
		});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletedevice", "editdevicecustom", "editdevice", "editdeviceparams", "overridedeviceparams"}, session) %>

<%
local id = cfe({type="hidden", value=".callerid"})
local comparison = cfe({type="hidden", value="~"})
local value = cfe({label="Caller ID"})
local display = cfe({type="hidden", value={"group", "param", "value"}})
local form = cfe({type="form", label="Search by Caller ID", value={id=id, comparison=comparison, value=value, display=display}, action="searchdevices", option="Search"})

if session.searchdevicesresult then
	local data = session.searchdevicesresult
	session.searchdevicesresult = nil

	if data.value and data.value.result then
		form.value.result = data.value.result
	end
	if data.value and data.value.value then
		form.value.value.value = data.value.value.value
	end
	if data.value and data.value.display then
		form.value.display = data.value.display
		form.value.display.type = "hidden"
	end
end

local func = haserl.loadfile(page_info.viewfile:gsub("searchbycallerid", "searchdevicedetails"))
func(form, viewlibrary, page_info, session)
%>
