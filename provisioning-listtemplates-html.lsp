<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
        $(document).ready(function() {
                $("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		$(".deletetemplate").click(function(){ return confirm("Are you sure you want to delete this template?")});
        });
</script>

<% htmlviewfunctions.displaycommandresults({"deletetemplate", "edittemplate"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createtemplate"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>File Name</th>
		<th>Label</th>
		<th>Sequence</th>
		<th>File Size</th>
		<th>Last Modified</th>
	</tr>
</thead><tbody>
<% local filename = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,file in ipairs( view.value ) do %>
	<tr>
		<td>
		<% filename.value = file.filename %>
		<% if viewlibrary.check_permission("edittemplate") and file.label then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Edit", action="edittemplate"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("deletetemplate") and file.label then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={filename=filename}, label="", option="Delete", action="deletetemplate", class="deletetemplate"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("createtemplate") and not file.label then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Create", action="createtemplate"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(file.filename) %></td>
		<td><%= html.html_escape(file.label) %></td>
		<td><%= html.html_escape(file.seq) %></td>
		<td><span class="hide"><%= html.html_escape(file.size or 0) %>b</span><%= format.formatfilesize(file.size) %></td>
		<td><%= format.formattime(file.mtime) %></td>
	</tr>
<% end %>
</tbody>
</table>

<% if view.errtxt then %>
<p class="error"><%= html.html_escape(view.errtxt) %></p>
<% end %>
<% if #view.value == 0 then %>
<p>No templates found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createtemplate") then
        viewlibrary.dispatch_component("createtemplate")
end %>
