APP_NAME=provisioning
PACKAGE=acf-$(APP_NAME)
VERSION=0.10.1

APP_DIST=\
	provisioning* \
	upgradeprovisioning \

CONFIG_DIST = config/*

CGIBIN_DIST = cgi-bin/*

LOGROTATE_FILES = provisioning

EXTRA_DIST=README Makefile config.mk

DISTFILES=$(APP_DIST) $(EXTRA_DIST) $(CONFIG_DIST) $(CGIBIN_DIST) $(LOGROTATE_FILES)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(appdir)/$(APP_NAME)
config_dir=$(DESTDIR)/etc/provisioning
cgibin_dir=$(DESTDIR)/var/www/provisioning/htdocs/cgi-bin
logrotate_dir=$(DESTDIR)/etc/logrotate.d

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p "$(install_dir)"
	cp -a $(APP_DIST) "$(install_dir)"
	mkdir -p "$(config_dir)"
	cp -a $(CONFIG_DIST) "$(config_dir)"
	mkdir -p "$(cgibin_dir)"
	cp -a $(CGIBIN_DIST) "$(cgibin_dir)"
	mkdir -p "$(logrotate_dir)"
	cp -a $(LOGROTATE_FILES) "$(logrotate_dir)"
	chmod 700 $(install_dir)/upgradeprovisioning

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	cp -a $(DISTFILES) $(P)
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
