<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<%
if form.value.values then
	-- First, display the serialized values
	local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Device"}), page_info)
	htmlviewfunctions.displayinfo(form)
	htmlviewfunctions.displayitem(cfe({label="Device ID", readonly=true, value=form.value.result.value[1].device_id}), page_info)
	local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Parameter Values"}), page_info, htmlviewfunctions.incrementheader(header_level))
	val = require("session").serialize("values", form.value.values.value)
	val = string.gsub(val, "[^\n]*%{%}\n", "")
	print("<pre>"..val.."</pre>")
	htmlviewfunctions.displaysectionend(header_level2)

	-- Then, display the templated values
	htmlviewfunctions.displaysectionstart(cfe({label="Configuration File"}), page_info, header_level2)
print("<xmp>")
	local func = haserl.loadfile(page_info.viewfile:gsub("getdevicevalues%-html", "templated"))
	func(form, viewlibrary, page_info, session)
print("</xmp>")
	htmlviewfunctions.displaysectionend(header_level2)
	htmlviewfunctions.displaysectionend(header_level)

	form.value.values = nil
end

form.value.result = nil
htmlviewfunctions.displayitem(form, page_info)
%>
