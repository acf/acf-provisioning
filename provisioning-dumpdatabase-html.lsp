<% local form, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if form.value.data then %>
<% local header_level = htmlviewfunctions.displaysectionstart(form.value.data, page_info) %>
<textarea name="filecontent">
<%= html.html_escape(form.value.data.value) %>
</textarea>
<% htmlviewfunctions.displaysectionend(header_level) %>
<% end %>

<%
	form.value.data = nil
	htmlviewfunctions.displayitem(form, page_info)
%>
