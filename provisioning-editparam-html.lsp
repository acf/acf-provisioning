<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<%
	htmlviewfunctions.displaycommandresults({"editoptions"}, session, true)
	htmlviewfunctions.displayitem(form, page_info)
	if form.value and form.value.param_id and viewlibrary.check_permission("editoptions") then
		local redir = cfe({ type="hidden", value=page_info.orig_action.."?param_id="..html.url_encode(form.value.param_id.value) })
		local param_id = cfe({type="hidden", value=form.value.param_id.value})
		htmlviewfunctions.displayitem(cfe({type="link", value={param_id=param_id, redir=redir}, label="Options", option="Edit Options", action="editoptions"}), page_info)
	end
%>
