<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% local action = "listdevices"
if page_info.action == "detaildevices" then action = "detaildevices" end %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.widgets.js"><\/script>');
		document.write('<link href="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/jquery.tablesorter.pager.css" rel="stylesheet">');
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/widgets/widget-pager.js"><\/script>');
	}
</script>

<script type="text/javascript">
<% -- Since we're including createdevice as a component, we break the automatic redirect
	if session.createdeviceresult and not session.createdeviceresult.errtxt then
		local tmp = session.createdeviceresult
		session.createdeviceresult = nil
		-- Use JavaScript to redirect to the edit page
%>
		window.location.assign("<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller) %>/<% if viewlibrary.check_permission("editdevicecustom") then io.write("editdevicecustom") elseif viewlibrary.check_permission("overridedeviceparams") then io.write("overridedeviceparams") elseif viewlibrary.check_permission("editdeviceparams") then io.write("editdeviceparams") else io.write("editdevice") end %>?device_id=<%= tmp.value.device_id.value %>&redir=<%= html.html_escape(page_info.orig_action) %>");
<%
	end
%>

        $(document).ready(function() {
		var display = ["<%= table.concat(view.value.display.value, '", "') %>"];
		<% if not view.value.display.overridedefault then %>
		var displaystorage = localStorage.getItem("DisplayColumns");
		if (displaystorage) {
			display = JSON.parse(displaystorage);
			// We don't update the select boxes here, because tablesorter resets them to default for some reason
		}
		<% end %>

		// The following is a hack to include a multiline string
		var MultiString = function(f) {
			return f.toString().split('\n').slice(1, -1).join('\n');
		}
		var actions = MultiString(function() {/**
		<%
			local device_id = cfe({ type="hidden", value="REPLACEME" })
			local redir = cfe({ type="hidden", value=page_info.orig_action })
			if viewlibrary.check_permission("editdevicecustom") then
				print('<form action="editdevicecustom" id="editdevicecustom" method="post"><input class=" hidden" type="hidden" name="redir" value="'..page_info.orig_action..'"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class=" submit" alt="Edit Classes" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/categories/applications-system.png" style="width:16px;height:16px;" title="Edit Device"></form>')
			else
				if viewlibrary.check_permission("editdevice") then
					print('<form action="editdevice" id="editdevice" method="post"><input class=" hidden" type="hidden" name="redir" value="'..page_info.orig_action..'"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class=" submit" alt="Edit Classes" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/categories/applications-system.png" style="width:16px;height:16px;" title="Edit Classes"></form>')
				end
				if viewlibrary.check_permission("overridedeviceparams") then
					print('<form action="overridedeviceparams" id="overridedeviceparams" method="post"><input class=" hidden" type="hidden" name="redir" value="'..page_info.orig_action..'"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class=" submit" alt="Edit Params" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/actions/document-properties.png" style="width:16px;height:16px;" title="Edit Params"></form>')
				elseif viewlibrary.check_permission("editdeviceparams") then
					print('<form action="editdeviceparams" id="editdeviceparams" method="post"><input class=" hidden" type="hidden" name="redir" value="'..page_info.orig_action..'"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class=" submit" alt="Edit Params" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/actions/document-properties.png" style="width:16px;height:16px;" title="Edit Params"></form>')
				elseif viewlibrary.check_permission("viewdeviceparams") then
					print('<form action="viewdeviceparams" id="viewdeviceparams" method="post"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class=" submit" alt="View Params" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/actions/document-properties.png" style="width:16px;height:16px;" title="View Params"></form>')
				end
			end
			if viewlibrary.check_permission("getdevicevalues") then
				print('<form action="getdevicevalues" id="getdevicevalues" method="post"><input class=" hidden" type="hidden" name="redir" value="getdevicevalues"><input class=" hidden" type="hidden" name="value" value="REPLACEME"><input class=" hidden" type="hidden" name="id" value="device_id"><input type="image" class=" submit" name="submit" alt="View Config" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/places/network-server.png" style="width:16px;height:16px;" title="View Config"></form>')
			end
			if viewlibrary.check_permission("deletedevice") then
				print('<form action="deletedevice" id="deletedevice" method="post"><input class=" hidden" type="hidden" name="device_id" value="REPLACEME"><input type="image" class="deletedevice submit" name="submit" alt="Delete" src="'..html.html_escape(page_info.wwwprefix..page_info.staticdir)..'/tango/16x16/emblems/emblem-unreadable.png" style="width:16px;height:16px;" title="Delete"></form>')
			end
		%>
		**/});

                $("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra', 'filter', 'pager'], widgetOptions: {
			// Filtering is handled by the server
			filter_serversideFiltering: true,

			// We can put the page number and size here, display columns, filtering and sorting handled by pager_customAjaxUrl
			pager_ajaxUrl : '<%= html.html_escape(page_info.script .. page_info.orig_action) %>?viewtype=json&page={page+1}&pagesize={size}',

			// Modify the url after all processing has been applied to handle display columns, filtering and sorting
			pager_customAjaxUrl: function(table, url) {
				for (var d=0; d<display.length; d++) {
					url += "&display."+(d+1)+"="+display[d];
				}
				var form = $("form[action='<%= action %>']");
				$("input[name^='orderby.']").remove();
				var columns = ["action"];
				columns = columns.concat(display);
				var directions = ["asc", "desc"];
				for (var s=0; s<table.config.sortList.length; s++) {
					// 0=column number, 1=direction(0 is asc)
					if ((table.config.sortList[s][0] < columns.length) && (table.config.sortList[s][1] < directions.length)) {
						url += "&orderby."+(s+1)+".column="+columns[table.config.sortList[s][0]]+"&orderby."+(s+1)+".direction="+directions[table.config.sortList[s][1]]
						form.append("<input class='hidden' type='hidden' name='orderby."+(s+1)+".column' value='"+columns[table.config.sortList[s][0]]+"'>");
						form.append("<input class='hidden' type='hidden' name='orderby."+(s+1)+".direction' value='"+directions[table.config.sortList[s][1]]+"'>");
					}
				}
				$("input[name^='filter.']").remove();
				for (var f=0; f<table.config.pager.currentFilters.length; f++) {
					var filter = table.config.pager.currentFilters[f];
					if (filter.trim()) {
						url += "&filter."+(f+1)+".column="+columns[f]+"&filter."+(f+1)+".value="+encodeURIComponent(filter.trim());
						form.append("<input class='hidden' type='hidden' name='filter."+(f+1)+".column' value='"+columns[f]+"'>");
						form.append("<input class='hidden' type='hidden' name='filter."+(f+1)+".value' value='"+filter.trim()+"'>");
					}
				}
				return url;
			},

			// process ajax so that the following information is returned:
			// [ total_rows (number), rows (array of arrays), headers (array; optional) ]
			pager_ajaxProcessing: function(data){
				if (data && data.value && data.value.result) {
					rows = [];
					for ( r=0; r<data.value.result.value.length; r++) {
						row=[];
						row[0] = actions.replace(/REPLACEME/g, data.value.result.value[r].device_id);
						for (var d=0; d<display.length; d++) {
							row[d+1] = data.value.result.value[r][display[d]];
						}
						rows.push(row);
					}
					return [ parseInt(data.value.rowcount.value), rows];
				}
			}
		}})
		.bind('pagerComplete', function(e, c){
			$(".deletedevice").click(function(){ return confirm("Are you sure you want to delete this device?")});
		});
		var HandleHeaderChange = function() {
			// Update the display array and reset the table
			display = $.map($(".columnheader option:selected"), function(elem) { return elem.value });
			<% if not view.value.display.overridedefault then %>
			localStorage.setItem("DisplayColumns", JSON.stringify(display));
			<% end %>
			var form = $("form[action='<%= action %>']");
			$("input[name^='display.']").remove();
			$(".columnheader").each(function(index) {
				form.append("<input class='hidden' type='hidden' name='display."+(index+1)+"' value='"+display[index]+"'>");
			});
			$("#list").trigger('resetToLoadState');
		}
		<% if not view.value.display.overridedefault then %>
		if (displaystorage) {
			// Add or remove boxes as necessary
			if ($(".columnheader").length < display.length) {
				for (var i=display.length-$(".columnheader").length; i>0; i--) {
					var prev = $(".addcolumn").closest("th").prev();
					prev.after(prev.clone(true));
				}
			}
			if ($(".columnheader").length > display.length) {
				for (var i=$(".columnheader").length-display.length; i>0; i--) {
					$(".removecolumn").last().closest("th").remove();
				}
			}
			// Update the select boxes
			$(".columnheader").each(function(index) {
				$(this).val(display[index]);
			});
			HandleHeaderChange();
		}
		<% end %>
		$(".columnheader").change(HandleHeaderChange);
		$(".removecolumn").click( function() {
			// Remove the table column header
			$(this).closest("th").remove();
			HandleHeaderChange();
		});
		$(".addcolumn").click( function() {
			// Duplicate the previous column header
			var prev = $(this).closest("th").prev();
			var next = prev.clone(true);
			next.find(".columnheader").val("<%= view.value.display.value[1] %>");
			next.insertAfter(prev);
			HandleHeaderChange();
		});
        });
</script>

<% htmlviewfunctions.displaycommandresults({"deletedevice", "editdevicecustom", "editdevice", "editdeviceparams", "overridedeviceparams"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createdevice"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<%
local reversedisplay = {}
for i,d in ipairs(view.value.display.value) do
	reversedisplay[d] = i
end
local reversedirection={asc=0, desc=1}
local sortlist = {}
for i,o in ipairs(view.value.orderby.value) do
	sortlist[#sortlist+1] = "["..reversedisplay[o.column]..","..reversedirection[o.direction].."]"
end
%>
<table id="list" class="tablesorter" style="table-layout:fixed;" data-sortlist="[<%= table.concat(sortlist,",") %>]"><thead>
	<tr>
	<% local count = 0
	if viewlibrary.check_permission("editdevicecustom") then
		count = count + 1
	else
		if viewlibrary.check_permission("editdevice") then count = count + 1 end
		if viewlibrary.check_permission("overridedeviceparams") or viewlibrary.check_permission("editdeviceparams") or viewlibrary.check_permission("viewdeviceparams") then count = count + 1 end
	end
	if viewlibrary.check_permission("getdevicevalues") then count = count + 1 end
	if viewlibrary.check_permission("deletedevice") then count = count + 1 end

	local defaultfilter = {}
	for i,f in ipairs(view.value.filter.value) do
		defaultfilter[f.column] = f.value
	end
	%>
		<th class="filter-false remove sorter-false" style="width:<%= 24 * count %>px;">Action</th>
	<% for i,c in ipairs(view.value.display.value) do %>
		<th<% if defaultfilter[c] then io.write(' data-value="'..html.html_escape(defaultfilter[c])..'"') end %>><div style="display:flex;">
			<img class="removecolumn" src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-remove.png' width='16' height='16' title="Remove Column" >
		<%
			local display = view.value.display.value
			view.value.display.class = "columnheader"
			view.value.display.name = "display"
			view.value.display.value = c
			io.write(html.form.select(view.value.display))
			view.value.display.value = display
		%></div></th>
	<% end %>
		<th class="filter-false remove sorter-false" style="width:16px;">
			<img class="addcolumn" src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-add.png' width='16' height='16' title="Add Column" >
		</th>
	</tr>
</thead><tbody>
</tbody>
</table>

<div id="pager" class="pager">
	<form>
		Page: <select class="gotoPage"></select>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/first.png" class="first"/>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/prev.png" class="prev"/>
		<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/next.png" class="next"/>
		<img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tablesorter/last.png" class="last"/>
		<select class="pagesize">
			<option selected="selected" value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option value="40">40</option>
		</select>
	</form>
</div>

<% if view.errtxt then %>
<p class="error"><%= html.html_escape(view.errtxt) %></p>
<% end %>
<% if #view.value.result.value == 0 then %>
<p>No devices found</p>
<% else
	local inputs = {viewtype=cfe({ type="hidden", value="stream"})}
	for i,d in ipairs(view.value.display.value) do
		inputs["display."..i] = cfe({ type="hidden", value=d })
	end
	htmlviewfunctions.displayitem(cfe({type="form", value=inputs, label="", option="Download", action=action}), page_info, -1)
end %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if (page_info.action == "listdevices" or page_info.action == "detaildevices") and viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createdevice") then
        viewlibrary.dispatch_component("createdevice")
end %>
